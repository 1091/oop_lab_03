#-------------------------------------------------
#
# Project created by QtCreator 2013-12-19T23:00:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = oop_lab_03
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
    mainwindow.cpp \
    car/ccar.cpp \
    car/cwheels.cpp \
    car/cengine.cpp \
    car/cspeed.cpp \
    car/cmodel.cpp \
    widgets/ccarwidget.cpp \
    widgets/cmodelwidget.cpp

HEADERS  += mainwindow.h \
    car/ccar.h \
    car/cwheels.h \
    car/ccarcass.h \
    car/cengine.h \
    bodies/cminiwan.h \
    bodies/csedan.h \
    bodies/cwagon.h \
    bodies/ccoupe.h \
    wheels/cusualwheels.h \
    wheels/cwoodwheels.h \
    wheels/csquarewheels.h \
    wheels/ctubelesswheels.h \
    engines/chorseengine.h \
    engines/celectricengine.h \
    engines/creactiveengine.h \
    engines/cdieselengine.h \
    engines/cgasengine.h \
    car/cspeed.h \
    car/cmessage.h \
    car/cmodel.h \
    widgets/ccarwidget.h \
    widgets/cmodelwidget.h

FORMS    += mainwindow.ui
