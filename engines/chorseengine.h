#ifndef CHORSEENGINE_H
#define CHORSEENGINE_H

#include "car/cengine.h"

class cHorseEngine : public cEngine {
public:
    cHorseEngine() : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION) {}
    cHorseEngine(bool started) : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION, started) {}
private:
    static constexpr float ACCELERATION = 1.389;
    static constexpr float MAX_SPEED_KMH = 50;
};

#endif // CHORSEENGINE_H
