#ifndef CELECTRICENGINE_H
#define CELECTRICENGINE_H

#include "car/cengine.h"

class cElectricEngine : public cEngine {
public:
    cElectricEngine() : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION) {}
    cElectricEngine(bool started) : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION, started) {}
private:
    static constexpr float ACCELERATION = 2.778;
    static constexpr float MAX_SPEED_KMH = 80;
};

#endif // CELECTRICENGINE_H
