#ifndef CGASENGINE_H
#define CGASENGINE_H

#include "car/cengine.h"

class cGasEngine : public cEngine {
public:
    cGasEngine() : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION) {}
    cGasEngine(bool started) : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION, started) {}
private:
    static constexpr float ACCELERATION = 3.968;
    static constexpr float MAX_SPEED_KMH = 150;
};

#endif // CGASENGINE_H
