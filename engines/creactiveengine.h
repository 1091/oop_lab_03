#ifndef CREACTIVEENGINE_H
#define CREACTIVEENGINE_H

#include "car/cengine.h"

class cReactiveEngine : public cEngine {
public:
    cReactiveEngine() : cEngine(cSpeed::kmPhToMps(MAX_SPEED_KMH), ACCELERATION) {}
    cReactiveEngine(bool started) : cEngine(cSpeed::kmPhToMps(MAX_SPEED_KMH), ACCELERATION, started) {}
    bool isReactive(){
        return true;
    }
private:
    static constexpr float ACCELERATION = 27.78;
    static constexpr float MAX_SPEED_KMH = 10000.0;
};

#endif // CREACTIVEENGINE_H
