#ifndef CDIESELENGINE_H
#define CDIESELENGINE_H

#include "car/cengine.h"

class cDieselEngine : public cEngine{
public:
    cDieselEngine() : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION) {}
    cDieselEngine(bool started) : cEngine(kmPhToMps(MAX_SPEED_KMH), ACCELERATION, started) {}
private:
    static constexpr float ACCELERATION = 5.556;
    static constexpr float MAX_SPEED_KMH = 100;
};

#endif // CDIESELENGINE_H
