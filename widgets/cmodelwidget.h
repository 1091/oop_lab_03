#ifndef CMODELWIDGET_H
#define CMODELWIDGET_H

#include <QVBoxLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>
#include <QString>
#include <QThread>
#include <QLCDNumber>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>

#include "car/cmodel.h"
#include "widgets/ccarwidget.h"

class cModelWidget : public QVBoxLayout, public cModel {
Q_OBJECT
public:
	cModelWidget(cCarWidget* carWidget);
public slots:
	void start();
	void carCreated();
	void carDeleted();	
private:
	bool isCarCreated;
	bool modelisClear;
	QSpinBox* timeBox;
	QPushButton* startButton;
	QTableWidget* modelTable;
	QLabel* infoLabel;
	void modeling();
	void clearModel();
	enum {
		COLUMNS = 3
	};
	enum Columns {
		COLUMN_SECONDS,
		COLUMN_SPEED,
		COLUMN_MESSAGE
	};
};

#endif // CMODELWIDGET_H
