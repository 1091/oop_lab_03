#include "cmodelwidget.h"

cModelWidget::cModelWidget(cCarWidget* carWidget) : cModel(carWidget){
	this->isCarCreated = false;
	this->modelisClear = true;
	QLabel* timeLabel = new QLabel("Modeling time:");
	this->timeBox = new QSpinBox;
	this->startButton = new QPushButton("Start Modeling");
	this->infoLabel = new QLabel;

	this->modelTable = new QTableWidget;
	modelTable->setMinimumWidth(600);
	modelTable->setColumnCount(COLUMNS);
	modelTable->setColumnWidth(COLUMN_SECONDS, 30);
	modelTable->setColumnWidth(COLUMN_SPEED, 100);
	modelTable->setColumnWidth(COLUMN_MESSAGE, 400);
	modelTable->verticalHeader()->setVisible(false);
	modelTable->horizontalHeader()->setVisible(false);
	modelTable->verticalScrollBar()->setVisible(false);

	this->addWidget(timeLabel);
	this->addWidget(timeBox);
	this->addWidget(startButton);
	this->addWidget(infoLabel);
	this->addWidget(modelTable);

	connect(startButton, SIGNAL(clicked()), this, SLOT(start()));
	connect(carWidget, SIGNAL(created()), this, SLOT(carCreated()));
	connect(carWidget, SIGNAL(deleted()), this, SLOT(carDeleted()));	
}

void cModelWidget::start(){
	if (!modelisClear){
		clearModel();
	} else {
		if (timeBox->value() == 0){
			infoLabel->setText("Error: time is 0");
		}
		if (!isCarCreated) {
			infoLabel->setText("Error: car not created");
		} else {
			timeOfModeling = timeBox->value();
			modelTable->setRowCount(timeOfModeling);
			modeling();
			modelisClear = false;
			startButton->setText("Clear the model");
		}
	}
}

void cModelWidget::modeling(){
	while (modelingIteration()){
		if (car->isWheelsBroken()){
			infoLabel->setText(QString::fromStdString(car->getMessage()));
		}
		QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(currentTime));
		newItem->setSizeHint(QSize(1, 10));
		modelTable->setItem(currentTime-1, 0, newItem);
		newItem = new QTableWidgetItem(tr("%1").arg(car->getSpeedKMH()));
		modelTable->setItem(currentTime-1, 1, newItem);
		newItem = new QTableWidgetItem(tr("%1").arg(QString::fromStdString(car->getMessage())));
		modelTable->setItem(currentTime-1, 2, newItem);
	}
}

void cModelWidget::clearModel(){
	modelTable->clear();
	currentTime = 0;
	car->nullifySpeed();
	modelisClear = true;
	startButton->setText("Start Modeling");
	infoLabel->setText("");
}

void cModelWidget::carCreated(){
	isCarCreated = true;
}

void cModelWidget::carDeleted(){
	isCarCreated = false;
}
