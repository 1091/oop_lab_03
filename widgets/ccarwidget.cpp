#include "ccarwidget.h"

cCarWidget::cCarWidget() : cCar(){
	this->carCreated = false;
	this->engineBox = new QComboBox;
	this->wheelsBox = new QComboBox;
	this->carcassBox = new QComboBox;
	this->carButton = new QPushButton("Create car");
	QLabel *carLabel = new QLabel("Car:");
	this->currentEngine = new QLabel;
	this->currentWheels = new QLabel;
	this->currentCarcass = new QLabel;
	QLabel *engineLabel = new QLabel("Engine:");
	this->engineButton = new QPushButton;
	this->engineState = new QLabel;
	QLabel *wheelsLabel = new QLabel("Wheels:");
	this->wheelsButton = new QPushButton;
	this->wheelsState = new QLabel;

	engineBox->addItem("Diesel engine", QVariant (DIESEL_ENGINE));
	engineBox->addItem("Electric engine", QVariant (ELECTRIC_ENGINE));
	engineBox->addItem("Gas engine", QVariant (GAS_ENGINE));
	engineBox->addItem("Horse engine", QVariant (HORSE_ENGINE));
	engineBox->addItem("Reactive engine", QVariant (REACTIVE_ENGINE));

	wheelsBox->addItem("Square wheels", QVariant (SQUARE_WHEELS));
	wheelsBox->addItem("Tubeless wheels", QVariant (TUBELESS_WHEELS));
	wheelsBox->addItem("Usual wheels", QVariant (USUSAL_WHEELS));
	wheelsBox->addItem("Wood wheels", QVariant (WOOD_WHEELS));

	carcassBox->addItem("Coupe", QVariant (COUPE_CARCASS));
	carcassBox->addItem("Mini wan", QVariant (MINIWAN_CARCASS));
	carcassBox->addItem("Sedan", QVariant (SEDAN_CARCASS));
	carcassBox->addItem("Wagon", QVariant (WAGON_CARCASS));

	this->addWidget(engineBox);
	this->addWidget(wheelsBox);
	this->addWidget(carcassBox);
	this->addWidget(carButton);
	this->addWidget(carLabel);
	this->addWidget(currentEngine);
	this->addWidget(currentWheels);
	this->addWidget(currentCarcass);
	this->addWidget(engineLabel);
	this->addWidget(engineButton);
	this->addWidget(engineState);
	this->addWidget(wheelsLabel);
	this->addWidget(wheelsButton);
	this->addWidget(wheelsState);

	connect(carButton, SIGNAL(clicked()), this, SLOT(manageCar()));
	connect(engineButton, SIGNAL(clicked()), this, SLOT(changeEngineState()));
	connect(wheelsButton, SIGNAL(clicked()), this, SLOT(inflateWheels()));	
}

void cCarWidget::manageCar(){
	if (!carCreated){
		cEngine* Engine;
		cWheels* Wheels;
		cCarcass* Carcass;
		switch (engineBox->currentIndex()){
			case DIESEL_ENGINE:{
				Engine = new cDieselEngine();
				break;
			}
			case ELECTRIC_ENGINE:{
				Engine = new cElectricEngine();
				break;
			}
			case GAS_ENGINE:{
				Engine = new cGasEngine();
				break;
			}
			case HORSE_ENGINE:{
				Engine = new cHorseEngine();
				break;
			}
			case REACTIVE_ENGINE:{
				Engine = new cReactiveEngine();
				break;
			}
		}
		switch (wheelsBox->currentIndex()){
			case SQUARE_WHEELS:{
				Wheels = new cSquareWheels();
				break;
			}
			case TUBELESS_WHEELS:{
				Wheels = new cTubelessWheels();
				break;
			}
			case USUSAL_WHEELS:{
				Wheels = new cUsualWheels();
				break;
			}
			case WOOD_WHEELS:{
				Wheels = new cWoodWheels();
				break;
			}
		}
		switch (carcassBox->currentIndex()){
			case COUPE_CARCASS:{
				Carcass = new cCoupe();
				break;
			}
			case MINIWAN_CARCASS:{
				Carcass = new cMiniWan();
				break;
			}
			case SEDAN_CARCASS:{
				Carcass = new cSedan();
				break;
			}
			case WAGON_CARCASS:{
				Carcass = new cWagon();
				break;
			}
		}
		if(Engine->isReactive() && !Carcass->isReactiveAcc()){
			currentEngine->setText("Engine can not be placed in this carcass type");
		} else {
			setEngine(Engine);
			setWheels(Wheels);
			setCarcass(Carcass);
			carButton->setText("Delete car");
			engineButton->setText("Start engine");
			currentEngine->setText(engineBox->currentText());
			currentWheels->setText(wheelsBox->currentText());
			currentCarcass->setText(carcassBox->currentText());
			if (wheels->isInflatable()){
				wheelsButton->setText("Inflate wheels");
				wheelsState->setText("Not inflated");
			}
			carCreated = true;
			emit created();
		}
	} else {
		clearCar();
		carCreated = false;
		carButton->setText("Create car");
		engineButton->setText("");
		engineState->clear();
		currentEngine->clear();
		currentWheels->clear();
		currentCarcass->clear();
		wheelsButton->setText("");
		wheelsState->clear();
		emit deleted();
	}
}

void cCarWidget::changeEngineState(){
	if (carCreated){
		if (engine->isStarted()){
			engine->killEngine();
			engineButton->setText("Start engine");
			engineState->setText("Engine is OFF");
		} else {
			engine->startEngine();
			engineButton->setText("Kill engine");
			engineState->setText("Engine is ON");
		}
	} else {
		currentEngine->setText("Car not created!!!");
	}
}

void cCarWidget::inflateWheels(){
	if (carCreated){
		if (wheels->isInflatable()){
			wheels->inflate();
			wheelsButton->setText("");
			wheelsState->setText("Inflated");
		} else {
			wheelsState->setText("Wheels is not inflatable");
		}
	} else {
		currentEngine->setText("Car not created!!!");
	}
}

void cCarWidget::disableCarWidget(){
	this->setEnabled(false);
}

void cCarWidget::enableCarWidget(){
	this->setEnabled(true);
}
