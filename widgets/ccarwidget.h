#ifndef CCARWIDGET_H
#define CCARWIDGET_H

#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QVariant>
#include <QLabel>

#include "car/ccar.h"

#include "engines/cdieselengine.h"
#include "engines/celectricengine.h"
#include "engines/cgasengine.h"
#include "engines/chorseengine.h"
#include "engines/creactiveengine.h"

#include "bodies/ccoupe.h"
#include "bodies/cminiwan.h"
#include "bodies/csedan.h"
#include "bodies/cwagon.h"

#include "wheels/csquarewheels.h"
#include "wheels/ctubelesswheels.h"
#include "wheels/cusualwheels.h"
#include "wheels/cwoodwheels.h"

class cCarWidget : public QVBoxLayout, public cCar {
Q_OBJECT
public:
	cCarWidget();
public slots:
	void manageCar();
	void disableCarWidget();
	void enableCarWidget();
	void changeEngineState();
	void inflateWheels();
signals:
	void created();
	void deleted();
private:
	bool carCreated;
	QComboBox* engineBox;
	QComboBox* wheelsBox;
	QComboBox* carcassBox;
	QPushButton* carButton;
	QLabel* currentEngine;
	QLabel* currentWheels;
	QLabel* currentCarcass;
	QPushButton* engineButton;
	QPushButton* wheelsButton;
	QLabel* engineState;
	QLabel* wheelsState;

	enum Engines {
		DIESEL_ENGINE,
		ELECTRIC_ENGINE,
		GAS_ENGINE,
		HORSE_ENGINE,
		REACTIVE_ENGINE
	};
	enum Wheels {
		SQUARE_WHEELS,
		TUBELESS_WHEELS,
		USUSAL_WHEELS,
		WOOD_WHEELS
	};
	enum Carcasses {
		COUPE_CARCASS,
		MINIWAN_CARCASS,
		SEDAN_CARCASS,
		WAGON_CARCASS
	};
};

#endif // CCARWIDGET_H
