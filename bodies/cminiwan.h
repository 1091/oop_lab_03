#ifndef CMINIWAN_H
#define CMINIWAN_H

#include "car/ccarcass.h"

class cMiniWan : public cCarcass {
public:
    cMiniWan() {}
    ~cMiniWan() {}
    bool isReactiveAcc(){
        return true;
    }
};

#endif // CMINIWAN_H
