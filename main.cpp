#include "mainwindow.h"

#include <QApplication>
#include <QComboBox>
#include <QVBoxLayout>

#include "widgets/ccarwidget.h"
#include "widgets/cmodelwidget.h"

int main(int argc, char *argv[]){

    QApplication a(argc, argv);
	QWidget window;
	cCarWidget* carWidget = new cCarWidget;
	cModelWidget* modelWidget = new cModelWidget(carWidget);
	QVBoxLayout* mainLayout = new QVBoxLayout;
	mainLayout->addLayout(carWidget);
	mainLayout->addLayout(modelWidget);
	window.setLayout(mainLayout);
	window.show();

    return a.exec();
}
