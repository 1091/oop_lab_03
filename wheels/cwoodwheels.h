#ifndef CWOODWHEELS_H
#define CWOODWHEELS_H

#include "car/cwheels.h"

class cWoodWheels : public cWheels {
public:
    cWoodWheels() : cWheels(kmPhToMps(MAX_ACC_SPEED)) {}
private:
    static constexpr float MAX_ACC_SPEED = 50;
};

#endif // CWOODWHEELS_H
