#ifndef CSQUAREWHEELS_H
#define CSQUAREWHEELS_H

#include "car/cwheels.h"

class cSquareWheels : public cWheels {
public:
    cSquareWheels() : cWheels() {}
    bool isFailure(float speed){
        setMessage("Square wheels failure");
        speed++;
        return true;
    }
};

#endif // CSQUAREWHEELS_H
