#ifndef CTUBELESSWHEELS_H
#define CTUBELESSWHEELS_H

#include "car/cwheels.h"

class cTubelessWheels : public cWheels {
public:
    cTubelessWheels(bool inflated) : cWheels(), inflated(inflated) {}
    cTubelessWheels() : cWheels(), inflated(false) {}
    bool isFailure(float speed){
        if (!inflated){
            setMessage("Tubeless wheels is not inflated");
            return true;
        }
        setMessage("");
        return this->cWheels::isFailure(speed);
    }
	bool isInflatable(){
		return true;
	}
	void inflate(){
		inflated = true;
	}
private:
    bool inflated;
};

#endif // CTUBELESSWHEELS_H
