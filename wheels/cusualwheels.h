#ifndef CUSUALWHEELS_H
#define CUSUALWHEELS_H

#include "car/cwheels.h"
#include <cstdlib>

class cUsualWheels : public cWheels {
public:
    cUsualWheels(bool inflated) : cWheels(), inflated(inflated) {}
    cUsualWheels() : cWheels(), inflated(false) {}
    bool isFailure(float speed){
        if (!inflated){
            setMessage("Usual wheels is not inflated");
            return true;
        }
        if (rand()%10 == 0){
            setMessage("Usual wheels random failure");
            brokeWheels();
            return true;
        }
        setMessage("");
        return this->cWheels::isFailure(speed);
    }
	bool isInflatable(){
		return true;
	}
	void inflate(){
		inflated = true;
	}
private:
    bool inflated;
};

#endif // CUSUALWHEELS_H
