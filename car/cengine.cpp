#include "cengine.h"

cEngine::cEngine(float maxSpeed, float acceleration, bool started){
    this->maxSpeed = maxSpeed;
    this->acceleration = acceleration;
    this->started = started;
}

cEngine::cEngine(float maxSpeed, float acceleration){
    this->maxSpeed = maxSpeed;
    this->acceleration = acceleration;
    this->started = false;
}

bool cEngine::isReactive(){
    return false;
}

void cEngine::startEngine(){
    this->started = true;
}

void cEngine::killEngine(){
    this->started = false;
}

float cEngine::getMaxSpeed(){
    return this->maxSpeed;
}

float cEngine::getAcceleration(){
    return this->acceleration;
}

bool cEngine::isStarted(){
    return this->started;
}

bool cEngine::isSpeedAboveMax(float speed){
    if (speed > maxSpeed){
        return true;
    } else {
        return false;
    }
}
