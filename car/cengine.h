#ifndef CENGINE_H
#define CENGINE_H

#include "cspeed.h"

class cEngine : public cSpeed {
public:
    cEngine(float maxSpeed, float acceleration, bool started);
    cEngine(float maxSpeed, float acceleration);
    virtual ~cEngine() {}
    void startEngine();
    void killEngine();
    float getMaxSpeed();
    float getAcceleration();
    bool isStarted();
    bool isSpeedAboveMax(float speed);
    virtual bool isReactive();
private:
    bool started;
    float maxSpeed;
    float acceleration;
};

#endif // CENGINE_H
