#include "cwheels.h"

cWheels::cWheels(){
    this->maxAccSpeed = kmPhToMps(SPEED_ON_FIRE);
    broken = false;
}

cWheels::cWheels(float maxAccSpeed){
    this->maxAccSpeed = maxAccSpeed;
    broken = false;
}

bool cWheels::isFailure(float speed){
    if (broken){
        setMessage("Wheels is broken");
        return true;
    }
    if (speed > maxAccSpeed){
        broken = true;
        setMessage("Wheels reached the maximum speed and have broken");
        return true;
    }
    setMessage("");
    return false;
}

void cWheels::brokeWheels(){
    this->broken = true;
}

bool cWheels::isInflatable(){
	return false;
}

void cWheels::inflate(){

}
