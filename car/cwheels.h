#ifndef CWHEELS_H
#define CWHEELS_H

#include "cspeed.h"
#include "cmessage.h"

class cWheels : public cSpeed, public cMessage {
public:
    cWheels();
    cWheels(float maxAccSpeed);
    virtual ~cWheels() {}
    virtual bool isFailure(float speed);
	virtual bool isInflatable();
	virtual void inflate();
protected:
	void brokeWheels();
private:	
    float maxAccSpeed;
    bool broken;
    static constexpr float SPEED_ON_FIRE = 1000;
};

#endif // CWHEELS_H
