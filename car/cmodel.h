#ifndef CMODEL_H
#define CMODEL_H

#include "ccar.h"
#include "cmessage.h"

class cModel : public cMessage{
public:
	cModel(cCar* car, int timeOfModeling);
	cModel(cCar* car);
	bool modelingIteration();
protected:
	int timeOfModeling;
	int currentTime;
	cCar* car;
	enum {
		ONE_SECOND = 1
	};
};

#endif // CMODEL_H
