#ifndef CSPEED_H
#define CSPEED_H

class cSpeed
{
public:
    cSpeed() {}
    static float kmPhToMps(float kmPh){
        return K*kmPh;
    }
    static float mPsToKmPh(float mPs){
        return mPs/K;
    }

private:
    static constexpr float K = 0.277777777777778;
};

#endif // CSPEED_H
