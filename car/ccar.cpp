#include "ccar.h"

cCar::cCar(cEngine* engine, cCarcass* carcass, cWheels* wheels){
	this->engine = engine;
    this->carcass = carcass;
    this->wheels = wheels;
    this->speed = 0;
}

cCar::cCar(){
	this->engine = NULL;
	this->carcass = NULL;
	this->wheels = NULL;
	this->speed = 0;
}

cCar::~cCar(){
	delete this->engine;
	delete this->wheels;
	delete this->carcass;
}

bool cCar::changeSpeed(int deltaTimeSeconds){
    speed += deltaTimeSeconds*engine->getAcceleration();
    if (!engine->isStarted()){
        speed = 0;
        setMessage("Engine is not started");
        return false;
    }
    if (wheels->isFailure(speed)){
        speed = 0;
        setMessage(wheels->getMessage());
        return false;
	}
	if (engine->isSpeedAboveMax(speed)){
		speed = engine->getMaxSpeed();
		setMessage("Maximum speed was reached");
		return false;
	}
    if (engine->isReactive() && !carcass->isReactiveAcc()){
        speed = 0;
        setMessage("Engine can not be placed in this carcass type");
        return false;
    }
    setMessage("");
    return true;
}

float cCar::getSpeed(){
    return this->speed;
}

float cCar::getSpeedKMH(){
    return cSpeed::mPsToKmPh(this->speed);
}

void cCar::setEngine(cEngine* engine){
	this->engine = engine;
}

void cCar::setWheels(cWheels* wheels){
	this->wheels = wheels;
}

void cCar::setCarcass(cCarcass* carcass){
	this->carcass = carcass;
}

void cCar::nullifySpeed(){
	this->speed = 0;
}

bool cCar::isWheelsBroken(){
	return wheels->isFailure(this->speed);
}

bool cCar::clearCar(){
	if (engine != NULL){
		delete engine;
	} else {
		return false;
	}
	if (carcass != NULL){
		delete carcass;
	} else {
		return false;
	}
	if (wheels != NULL){
		delete wheels;
	} else {
		return false;
	}
	return true;
}
