#ifndef CMESSAGE_H
#define CMESSAGE_H

#include <string>

class cMessage {
public:
    cMessage() {}
    std::string getMessage(){
        return this->message;
    }    
protected:
	void setMessage(std::string message){
		this->message = message;
	}
    std::string message;
};

#endif // CMESSAGE_H
