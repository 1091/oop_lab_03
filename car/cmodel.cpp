#include "cmodel.h"

cModel::cModel(cCar* car, int timeOfModeling){
	this->car = car;
	this->timeOfModeling = timeOfModeling;
	this->currentTime = 0;
}

cModel::cModel(cCar* car){
	this->car = car;
	this->timeOfModeling = 0;
	this->currentTime = 0;
}

bool cModel::modelingIteration(){
	if (currentTime >= timeOfModeling){
		return false;
	}
	if (!car->changeSpeed(ONE_SECOND)){}
	currentTime++;
	return true;
}

