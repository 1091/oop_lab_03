#ifndef CCAR_H
#define CCAR_H

#include "cwheels.h"
#include "cengine.h"
#include "ccarcass.h"
#include "cmessage.h"

class cCar : public cSpeed, public cMessage {
public:
    cCar(cEngine* engine, cCarcass* carcass, cWheels* wheels);
	cCar();
	~cCar();
    bool changeSpeed(int deltaTimeSeconds);
    float getSpeedKMH();
	float getSpeed();
	void nullifySpeed();
	void setEngine(cEngine* engine);
	void setWheels(cWheels* wheels);
	void setCarcass(cCarcass* carcass);
	bool isWheelsBroken();
	bool clearCar();
protected:
	cWheels* wheels;
    cEngine* engine;
    cCarcass* carcass;
    float speed;
};

#endif // CCAR_H
